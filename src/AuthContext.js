import { createContext, useContext, useState } from 'react'

const AuthContext = createContext()

export const AuthProvider = ({ children }) => {
  const [authenticated, setAuthenticated] = useState(false)
  const [jwt, setJwt] = useState()
  const [msg, setMsg] = useState('')
  const [role, setRole] = useState('public')
  const [username, setUsername] = useState()

  const login = (username, password) => {
    console.log(username, password)
    // Implement your authentication logic here
    if (username === 'johndoe' && password === '1234') {
      setAuthenticated(true)
      setJwt('1234')
      setMsg('')
      setRole('ADMIN')
      setUsername(username)
    } else if (username === 'abu' && password === '1234') {
      setAuthenticated(true)
      setMsg('')
      setRole('AUTHOR')
      setUsername(username)
    } else {
      // show login failed message
      setMsg('Username and Password do not match')
    }
  }

  const logout = () => {
    setAuthenticated(false)
    window.location.href = '/'
  }

  return (
    <AuthContext.Provider
      value={{ authenticated, login, logout, jwt, msg, role, username }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export const useAuth = () => {
  return useContext(AuthContext)
}
