import { Link } from 'react-router-dom'
import { useAuth } from '../AuthContext'

function Menu() {
  let { logout, role, username } = useAuth()
  return (
    <nav
      className="navbar navbar-expand-lg bg-body-tertiary"
      data-bs-theme="dark"
    >
      <div className="container-fluid">
        <a className="navbar-brand" href="/">
          Navbar
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link to="/" className="nav-link active" aria-current="page">
                Home
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/post-list" className="nav-link">
                Post List
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/post-create" className="nav-link">
                Create Post
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/car-list" className="nav-link">
                Car List
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/car-new" className="nav-link">
                Create Car
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/mui-button" className="nav-link">
                Mui Button
              </Link>
            </li>
            {role === 'ADMIN' && (
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="/"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Car
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <Link to="/car-list" className="dropdown-item">
                      Listing
                    </Link>
                  </li>
                  <li>
                    <Link to="/car-new" className="dropdown-item">
                      New Car
                    </Link>
                  </li>
                </ul>
              </li>
            )}
          </ul>
          <form className="d-flex" role="search">
            <input
              className="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              className="btn btn-outline-success"
              type="submit"
              onClick={logout}
            >
              ( {username} )Logout
            </button>
          </form>
        </div>
      </div>
    </nav>
  )
}

export default Menu
