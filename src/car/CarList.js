import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import './CarList.css' // Import the CSS

function CarList() {
  let [cars, setCars] = useState([])
  let [ok, setOk] = useState(true)

  // useEffect, work like constructor
  useEffect(() => {
    fetchCar()
  }, [])

  function fetchCar() {
    fetch('http://localhost:8080/car-search')
      .then((response) => response.json())
      .then((json) => {
        console.log(json.body)
        setCars(json.body)
      })
      .catch((err) => {
        console.log(err)
        setOk(false)
      })
  }

  function deleteCar(id) {
    console.log('id = ', id)
    let yes = window.confirm('Are you sure ?')
    if (yes) {
      // delete the data
      let url = 'http://localhost:8080/car-delete/' + id
      let setting = {
        method: 'DELETE',
      }
      fetch(url, setting)
        .then((response) => response.json())
        .then((json) => {
          console.log(json)
          fetchCar()
        })
    }
  }

  function printCar() {
    return (
      <div className="table-container">
        <h4 className="centered-text">All cars</h4>
        {ok ? (
          <table className="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>Brand</th>
                <th>Model</th>
                <th>Color</th>
                <th>Registration No.</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {cars.map((item, index) => (
                <tr key={item.id}>
                  <td>{index + 1}</td>
                  <td>{item.brand}</td>
                  <td>{item.model}</td>
                  <td>{item.color}</td>
                  <td>{item.registerNumber}</td>

                  <td>
                    <div className="action-buttons">
                      <button
                        onClick={() => deleteCar(item.id)}
                        className="btn btn-danger"
                      >
                        Delete
                      </button>
                      <Link
                        to={`/post-edit/${item.id}`}
                        className="btn btn-outline-success"
                      >
                        Update
                      </Link>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <p className="error-message">Error during loading data...</p>
        )}
      </div>
    )
  }
  if (!ok) {
    return <p className="alert alert-danger">Error during loading data...</p>
  }

  return <div>{printCar()}</div>
}

export default CarList
