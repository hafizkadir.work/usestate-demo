import { useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import React from 'react'
import './CarForm.css'
import setting from '../setting'

function CarForm() {
  let model = useRef()
  let brand = useRef()
  let color = useRef()
  let year = useRef()
  let registerNumber = useRef()
  let price = useRef()
  let navigate = useNavigate()

  function save() {
    let url = setting.url + '/car-add'
    let data = {
      model: model.current.value,
      brand: brand.current.value,
      color: color.current.value,
      year: year.current.value,
      registerNumber: registerNumber.current.value,
      price: price.current.value,
    }
    let setting = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-type': 'application/json' },
    }

    fetch(url, setting)
      .then((response) => response.json())
      .then((json) => console.log(json))
    navigate('/car-list')
  }

  return (
    <div className="form-container">
      <h4>Please enter your car details</h4>
      <form>
        <input
          type="text"
          className="form-input"
          placeholder="Brand"
          ref={brand}
        />
        <input
          type="text"
          className="form-input"
          placeholder="Model"
          ref={model}
        />
        <input
          type="text"
          className="form-input"
          placeholder="Color"
          ref={color}
        />
        <input
          type="text"
          className="form-input"
          placeholder="Registration Number"
          ref={registerNumber}
        />
        <input
          type="text"
          className="form-input"
          placeholder="Price"
          ref={price}
        />
        <input
          type="text"
          className="form-input"
          placeholder="Year"
          ref={year}
        />
        <button className="form-button" onClick={save}>
          Save
        </button>
      </form>
    </div>
  )
}

export default CarForm
