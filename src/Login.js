import { Link } from 'react-router-dom'
import { useAuth } from './AuthContext'
import { useRef, useState } from 'react'

function Login() {
  const { login, msg } = useAuth()
  let username = useRef()
  let password = useRef()
  let [err, setErr] = useState({})

  function doLogin(event) {
    event.preventDefault()
    console.log(username.current.value, password.current.value)
    let err2 = {}
    if (username.current.value === '') {
      err2.username = 'Username is required'
    }

    if (password.current.value === '') {
      err2.password = 'Password is required'
    }

    setErr(err2)
    console.log(err2)
    if (Object.keys(err2).length === 0) {
      login(username.current.value, password.current.value)
    }
  }

  return (
    <form onSubmit={(e) => doLogin(e)}>
      <div className="card col-md-6 mx-auto" style={{ marginTop: '50px' }}>
        <div className="card-header">Login Screen</div>
        <div className="card-body">
          {msg !== '' && <p className="alert alert-danger">{msg}</p>}

          <div className="row">
            <div className="col-md-12">
              <label>User ID</label>
              <input type="text" className="form-control" ref={username} />
              {err.username && <p className="text-danger">{err.username}</p>}
            </div>
            <div className="col-md-12">
              <label>Password</label>
              <input type="password" className="form-control" ref={password} />
              {err.password && <p className="text-danger">{err.password}</p>}
            </div>
            <div className="col-md-12 mt-2">
              <button className="btn btn-primary me-2">Login</button>
              <Link to="/registration" className="btn btn-primary">
                Register
              </Link>
            </div>
          </div>
        </div>
      </div>
    </form>
  )
}

export default Login
