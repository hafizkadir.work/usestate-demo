import { useEffect, useRef } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import setting from '../setting'

function EditPost() {
  let author = useRef()
  let title = useRef()
  let id = useRef()
  let { id2 } = useParams()
  let navigate = useNavigate()

  useEffect(() => {
    // get a post
    fetch(setting.url2 + '/posts/' + id2)
      .then((rsponse) => rsponse.json())
      .then((json) => {
        author.current.value = json.author
        title.current.value = json.title
        id.current.value = json.id
      })
  }, [])

  function save() {
    console.log(author.current.value, title.current.value)
    let url = setting.url2 + '/posts/' + id.current.value
    let data = { author: author.current.value, title: title.current.value }
    let setting = {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: { 'Content-type': 'application/json' },
    }
    fetch(url, setting)
      .then((response) => response.json())
      .then((json) => console.log(json))
    navigate('/post-list')
  }

  return (
    <div>
      <input type="hidden" ref={id} />
      <div className="row">
        <div className="col-md-1">Title</div>
        <div className="col-md-5">
          <input type="text" className="form-control" ref={title} />
        </div>
      </div>
      <div className="row">
        <div className="col-md-1">Author</div>
        <div className="col-md-5">
          <input type="text" className="form-control" ref={author} />
        </div>
      </div>
      <div className="row">
        <div className="col-md-1"></div>
        <div className="col-md-5">
          <button className="btn btn-primary" onClick={save}>
            Save
          </button>
        </div>
      </div>
    </div>
  )
}

export default EditPost
