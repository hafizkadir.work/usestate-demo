import React, { useState, useEffect, useRef } from 'react'
import setting from '../setting'

function ListPost() {
  const [post, setPost] = useState([])
  let [ok, setOk] = useState(true)
  const [editingPost, setEditingPost] = useState(null)
  const [isEditModalOpen, setIsEditModalOpen] = useState(false)

  // Create and initialize the refs
  const editedTitle = useRef(null)
  const editedAuthor = useRef(null)

  useEffect(() => {
    fetch(setting.url2 + '/posts')
      .then((response) => response.json())
      .then((json) => {
        setPost(json)
      })
  }, [])

  function openEditModal(post) {
    setEditingPost(post)
    setIsEditModalOpen(true)

    // Check if the refs are not null before accessing 'value' property
    if (editedTitle.current && editedAuthor.current) {
      editedTitle.current.value = post.title
      editedAuthor.current.value = post.author
    }
  }

  function closeEditModal() {
    setIsEditModalOpen(false)
  }

  function saveEdits() {
    const id = editingPost.id
    const updatedTitle = editedTitle.current.value
    const updatedAuthor = editedAuthor.current.value

    // Make a PUT request to update the post on the server
    const url = 'http://localhost:9090/posts/' + id
    const data = { title: updatedTitle, author: updatedAuthor }
    const setting = {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: { 'Content-type': 'application/json' },
    }

    fetch(url, setting)
      .then((response) => response.json())
      .then((json) => {
        setPost((prevPost) =>
          prevPost.map((item) =>
            item.id === id
              ? { ...item, title: updatedTitle, author: updatedAuthor }
              : item
          )
        )
        closeEditModal()
      })
  }

  function deleteMe(id) {
    const yes = window.confirm('Are you sure?')
    if (yes) {
      const url = 'http://localhost:9090/posts/' + id
      const setting = {
        method: 'DELETE',
      }
      fetch(url, setting)
        .then((response) => response.json())
        .then((json) => {
          setPost((prevPost) => prevPost.filter((item) => item.id !== id))
        })
    }
  }

  function printPost() {
    return (
      <div>
        <table className="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <td>No.</td>
              <td>Title</td>
              <td>Author</td>
              <td>ID</td>
              <td>Action</td>
            </tr>
          </thead>
          <tbody>
            {post.map((item, index) => (
              <tr key={item.id}>
                <td>{index + 1}</td>
                <td>{item.title}</td>
                <td>{item.author}</td>
                <td>{item.id}</td>
                <td>
                  <div className="button-container">
                    <button
                      onClick={() => deleteMe(item.id)}
                      className="btn btn-outline-danger me-1"
                    >
                      Delete
                    </button>

                    <button
                      onClick={() => openEditModal(item)}
                      className="btn btn-outline-warning ms-1"
                    >
                      Edit post
                    </button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }

  if (!ok) return <p className="alert alert-danger"> Error Loading Data</p>

  return (
    <div>
      {isEditModalOpen && (
        <div className="edit-modal">
          <div className="edit-modal-content">
            <h2>Edit Post</h2>
            <div className="form-group">
              <label htmlFor="editedTitle">Title:</label>
              <input
                type="text"
                className="form-control"
                id="editedTitle"
                ref={editedTitle}
              />
            </div>
            <div className="form-group">
              <label htmlFor="editedAuthor">Author:</label>
              <input
                type="text"
                className="form-control"
                id="editedAuthor"
                ref={editedAuthor}
              />
            </div>
            <button className="btn btn-primary" onClick={saveEdits}>
              Save
            </button>
            <button className="btn btn-secondary" onClick={closeEditModal}>
              Cancel
            </button>
          </div>
        </div>
      )}

      {post.length === 0 ? <p>Loading...</p> : printPost()}
    </div>
  )
}

export default ListPost
